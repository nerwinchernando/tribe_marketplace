class DisplayBreakdown
  attr_accessor :total_cost, :bundle_cost, :bundle_breakdown, :format_code

  def initialize(format_code, number_of_items, total_cost, bundle_cost, bundle_breakdown)
    @format_code = format_code
    @number_of_items = number_of_items
    @total_cost = total_cost
    @bundle_cost = bundle_cost
    @bundle_breakdown = bundle_breakdown
  end

  def generate
    @total_cost = '%.2f' % @total_cost
    str = "#{@number_of_items} #{@format_code} $#{@total_cost}"

    @bundle_breakdown.each do |breakdown_of, total|
      if total > 0
        cost = '%.2f' % (@bundle_cost[breakdown_of] * total)
        str.concat("\n-- #{total} x #{breakdown_of} $#{cost}")
      end
    end
    str
  end
end