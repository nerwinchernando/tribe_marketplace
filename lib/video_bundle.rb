require 'compute_cost'
require 'display_breakdown'

class VideoBundle
  BUNDLES_COST = { 9 => 1530.00, 5 => 900.00, 3 => 570.00 }
  
  def initialize(number_of_items)
    @number_of_items = number_of_items
    @breakdowns = { 9 => 0, 5 => 0, 3 => 0 }
  end

  def determine_cost
    compute = ComputeCost.new(BUNDLES_COST, @breakdowns, @number_of_items)
    compute.execute
    total_cost = compute.total_cost
  end

  def get_breakdown
    total_cost = '%.2f' % determine_cost
    display_breakdown = DisplayBreakdown.new("VID", @number_of_items, total_cost, BUNDLES_COST, @breakdowns)
    str = display_breakdown.generate
  end

end