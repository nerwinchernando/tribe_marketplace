require 'image_bundle'
require 'audio_bundle'
require 'video_bundle'

class Submissions
  attr_accessor :line_items, :error

  INSTRUCTION_ACTIONS = {
    "IMG": :ImageBundle,
    "FLAC": :AudioBundle,
    "VID": :VideoBundle
  }
  def initialize
    @line_items = []
    @error = ""
  end

  def post(params)
    @parameters = params.split
    @parameters.each_slice(2) do |quantity, format_code|
      if is_valid?(quantity, format_code)
        @line_items << Object.const_get(INSTRUCTION_ACTIONS[format_code.upcase.to_sym]).new(quantity)
      end
    end
  end

  def display_breakdown
    string_breakdown = ""
    @line_items.each do |line_item|
      string_breakdown.concat(line_item.get_breakdown)
      if @line_items.last != line_item
        string_breakdown.concat("\n")
      end
    end
    string_breakdown
  end
  
  private
  
  def is_valid?(quantity, format_code)
    # It is not numeric but it is FLAC, VID, IMG
    if quantity.to_i == 0 && (['FLAC', 'VID', 'IMG']).include?(quantity.upcase)
      @error = "Missing quantity parameter"
      return false
    end

    # Totally not numeric or it is not pure integer
    if quantity.to_i.to_s != quantity
      @error = "Quantity parameter is not numeric"
      return false
    end

    if format_code.nil?
      @error = "Missing key or value pair"
      return false
    end

    if not ['FLAC', 'VID', 'IMG'].include? format_code.upcase
      @error = "Invalid format code"
      return false
    end

    return true
  end
end