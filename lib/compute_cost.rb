class ComputeCost
  attr_accessor :total_cost

  def initialize(bundle_cost, breakdowns, number_of_items)
    @total_cost = 0.00
    @number_of_items = number_of_items.to_i
    @bundle_cost = bundle_cost
    @breakdowns = breakdowns
  end

  def execute
    quantity = @number_of_items
    not_complete = true
    is_empty = true
    
    while not_complete
      previous_bundle = nil
      @bundle_cost.each do |bundle_of, cost|
        next if @bundle_cost[bundle_of] == nil
        
        if quantity >= bundle_of
          # compute using modulo
          remainder = quantity % bundle_of
          bundles   = quantity / bundle_of
          @breakdowns[bundle_of] = @breakdowns[bundle_of] + bundles

          if remainder == 0
            not_complete = false

            compute_total_cost
            break
          else
            raise_error_if_number_of_items_is_invalid(bundle_of)

            is_empty = false
            previous_bundle = bundle_of
          end

          quantity = remainder
        else
          if is_empty
            raise_error_if_number_of_items_is_invalid bundle_of
            next
          else
            # flag the bundle to nil
            @bundle_cost[previous_bundle] = nil
            quantity = @number_of_items

            reset_breakdowns
            break
          end
        end
      end
    end # while not complete
  end

private
  def raise_error_if_number_of_items_is_invalid(bundle_of)
      # if this is the last iteration but remainder is not 0
    if @bundle_cost.keys.last == bundle_of
      raise ArgumentError.new('You must specify correct number of items.')
    end
  end

  def reset_breakdowns
    @total_cost = 0
    @breakdowns.each do |bundle_of, total|
      @breakdowns[bundle_of] = 0
    end
  end

  def compute_total_cost
    @total_cost = 0
    @breakdowns.each do |bundle_of, total|
      if total > 0
        @total_cost = @total_cost + (@bundle_cost[bundle_of] * total)
      end
    end
  end
end