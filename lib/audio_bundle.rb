require 'compute_cost'
require 'display_breakdown'

class AudioBundle
  BUNDLES_COST = { 9 => 1147.50, 6 => 810.00, 3 => 427.50 }

  def initialize(number_of_items)
    @number_of_items = number_of_items
    @breakdowns = {9 => 0, 6 => 0, 3 => 0}
  end
  
  def determine_cost
    compute = ComputeCost.new(BUNDLES_COST, @breakdowns, @number_of_items)
    # Todo: call a method in compute to call the computation
    #       it should be able to raise errors as posssible
    #       also store the error like "The value of bundle is invalid"
    compute.execute
    total_cost = compute.total_cost
  end

  def get_breakdown
    total_cost = '%.2f' % determine_cost
    display_breakdown = DisplayBreakdown.new("FLAC", @number_of_items, total_cost, BUNDLES_COST, @breakdowns)
    str = display_breakdown.generate
  end

end
