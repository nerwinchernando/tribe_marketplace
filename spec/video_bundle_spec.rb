require 'video_bundle'

describe VideoBundle do
  describe '.determine_cost' do
  	it "determines the cost of 13 Videos" do
      video_bundle = VideoBundle.new("13")
      expect(video_bundle.determine_cost).to eq(2370.00)
    end
  end

  describe '.get_breakdown' do
    it "gets the breakdown of 13 Videos" do
      video_bundle = VideoBundle.new("13")
      expected = "13 VID $2370.00\n-- 2 x 5 $1800.00\n-- 1 x 3 $570.00"

      expect(video_bundle.get_breakdown).to eq(expected)
    end
  end
end