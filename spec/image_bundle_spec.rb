require 'image_bundle'

describe ImageBundle do
  describe ".determine_cost" do
    context "valid number of items" do
      it "determines the cost of 5 images" do
        image_bundle = ImageBundle.new("5")
        expect(image_bundle.determine_cost).to eq(450.00)
      end

      it "determines the cost of 10 images" do
        image_bundle = ImageBundle.new("10")
        expect(image_bundle.determine_cost).to eq(800.00)
      end

      it "determines the cost of 15 images" do
        image_bundle = ImageBundle.new("15")
        expect(image_bundle.determine_cost).to eq(1250.00)
      end
    end

    context "invalid number of items" do
      it "determines the cost of 4 images" do
        image_bundle = ImageBundle.new("4")
        expect { (image_bundle.determine_cost) }.to raise_error.with_message(/You must specify correct number of items./)
      end

      it "determines the cost of 16 images" do
        image_bundle = ImageBundle.new("16")
        expect { (image_bundle.determine_cost) }.to raise_error.with_message(/You must specify correct number of items./)
      end
    end
  end

  describe '.get_breakdown' do
    it "gets the breakdown of 5 images" do
      image_bundle = ImageBundle.new("5")
      expected = "5 IMG $450.00\n-- 1 x 5 $450.00"

      expect(image_bundle.get_breakdown).to eq(expected)
    end

    it "gets the breakdown of 10 images" do
      image_bundle = ImageBundle.new("10")
      expected = "10 IMG $800.00\n-- 1 x 10 $800.00"

      expect(image_bundle.get_breakdown).to eq(expected)
    end

    it "gets the breakdown of 15 images" do
      image_bundle = ImageBundle.new("15")
      expected = "15 IMG $1250.00\n-- 1 x 10 $800.00\n-- 1 x 5 $450.00"

      expect(image_bundle.get_breakdown).to eq(expected)
    end
  end
end
