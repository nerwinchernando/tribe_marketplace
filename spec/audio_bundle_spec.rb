require 'audio_bundle'

describe AudioBundle do
  describe '.determine_cost' do
    describe "invalid number of items" do
      it "determines the cost of 15 FLACs" do
        audio_bundle = AudioBundle.new("15")
        expect(audio_bundle.determine_cost).to eq(1957.50)
      end
    end
    
    describe "invalid number of items" do
      it "determines the cost of 2 FLACs" do
        audio_bundle = AudioBundle.new("2")
        expect { (audio_bundle.determine_cost) }.to raise_error.with_message(/You must specify correct number of items./)
      end

      it "determines the cost of 4 FLACs" do
        audio_bundle = AudioBundle.new("4")
        expect { (audio_bundle.determine_cost) }.to raise_error.with_message(/You must specify correct number of items./)
      end
    end
  end

  describe '.get_breakdown' do
    it "gets the breakdown of 15 FLACs" do
      audio_bundle = AudioBundle.new("15")
      expected = "15 FLAC $1957.50\n-- 1 x 9 $1147.50\n-- 1 x 6 $810.00"

      expect(audio_bundle.get_breakdown).to eq(expected)
    end
  end
end
