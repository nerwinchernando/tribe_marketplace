require 'submissions'

describe Submissions do
  let(:submissions) { Submissions.new }
  describe '.post' do
    it 'sets the line items' do
      submissions.post("10 IMG 15 FLAC 13 VID")

      expect(submissions.line_items.count).to eq(3)
    end
  end
  
  describe 'invalid parameters' do
    context 'missing key value pair' do
      it 'generates an error' do
        submissions.post("10 IMG 15 FLAC 13")
        expected = "Missing key or value pair"

        expect(submissions.error).to eq(expected)
      end
    end

    context 'quantity is not integer' do
      it 'generates an error' do
        submissions.post("10w IMG 15 FLAC 13 VID")
        expected = "Quantity parameter is not numeric"
      
        expect(submissions.error).to eq(expected)
      end
    end

    context 'quantity not provided' do
      it 'generates an error' do
        submissions.post("IMG 15 FLAC 13 VID")
        expected = "Missing quantity parameter"
      
        expect(submissions.error).to eq(expected)
      end
    end
    
    context 'invalid format code' do
      it 'generates an error' do
        submissions.post("10 MOV 15 FLAC 13 VID")
        expected = "Invalid format code"

        expect(submissions.error).to eq(expected)
      end
    end
  end

  describe '.display_breakdown' do
    it 'displays the breakdown of 10 IMG' do
      expected = "10 IMG $800.00\n-- 1 x 10 $800.00"
      
      submissions.post("10 IMg")
      expect(submissions.display_breakdown.strip).to eq(expected.strip)
    end

    it 'displays the breakdown of 15 FLAC' do
      expected = "15 FLAC $1957.50\n-- 1 x 9 $1147.50\n-- 1 x 6 $810.00"
      
      submissions.post("15 FLAc")
      expect(submissions.display_breakdown.strip).to eq(expected.strip)
    end

    it 'displays the breakdown of 13 VID' do
      expected = "13 VID $2370.00\n-- 2 x 5 $1800.00\n-- 1 x 3 $570.00"
      
      submissions.post("13 VID")
      expect(submissions.display_breakdown.strip).to eq(expected.strip)
    end

    it 'displays the breakdown of 10 IMG 15 FLAC 13 VID' do
      expected = "10 IMG $800.00\n-- 1 x 10 $800.00\n15 FLAC $1957.50\n-- 1 x 9 $1147.50\n-- 1 x 6 $810.00\n13 VID $2370.00\n-- 2 x 5 $1800.00\n-- 1 x 3 $570.00"
      
      submissions.post("10 IMG 15 FLAC 13 VID")
      expect(submissions.display_breakdown.strip).to eq(expected.strip)
    end

  end
end