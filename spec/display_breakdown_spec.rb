require 'display_breakdown'

describe DisplayBreakdown do
  it ".generate" do
    format_code = "IMG"
    number_of_items = 10
    total_cost = 800.00
    bundle_cost = { 10 => 800.00, 5 => 450.00 }
    bundle_breakdown = { 10 => 1, 5 => 0 }

   	expected = "10 IMG $800.00\n-- 1 x 10 $800.00"

    display_breakdown = DisplayBreakdown.new(format_code, number_of_items, total_cost, bundle_cost, bundle_breakdown)
    
    expect(display_breakdown.generate).to eq(expected)
  end
end
