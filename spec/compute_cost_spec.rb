require 'compute_cost'

describe ComputeCost do
  it "determines the cost depending from the bundles cost" do
    bundle_cost = { 10 => 800.00, 5 => 450 }
    bundle_breakdown = { 10 => 0, 5 => 0 }

    compute_cost = ComputeCost.new(bundle_cost, bundle_breakdown, 10)
    compute_cost.execute
    
    expect(compute_cost.total_cost).to eq(800.00)
  end
end
